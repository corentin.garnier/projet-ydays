# Projet Ydays :
Création d'un coinmarketcap qui liste des tokens.
Le but est d'utiliser une API pour réaliser le site, ou il sera resensé le prix moyen d'un nombre X de cryptomonnaies.

## Stack utilisé :
ReactJS pour le front.  
[Coingecko](https://www.coingecko.com/fr/api) pour l'API.

## Membres :
Personnes participant au projet : Manon FARGUES, Corentin GARNIER
