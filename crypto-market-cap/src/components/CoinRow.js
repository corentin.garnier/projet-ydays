import{React} from "react";
import {useNavigate} from "react-router-dom"
const CoinRow = ({coin, index, currency}) => {
    let navigate = useNavigate();

        function showDetails() {
            return navigate(`/paire?token=${coin.id}`);
        }

    return (
        <tr style={{cursor:'pointer'}} className="items" onClick={()=> showDetails()}>
            <td>{index}</td>
            <td>
                <img 
                    src={coin.image} 
                    alt="{coin.name}" 
                    style={{width: '3%'}} 
                    className="me-4"
                />
                <span>{coin.name}</span>
                <span className="ms-3 text-muted text-uppercase symbol">{coin.symbol}</span>
            </td>
            
            <td>{coin.current_price} {currency === 'usd' ? "$US" : "€"}</td>
            <td>{coin.market_cap} {currency === 'usd' ? "$US" : "€"}</td>
            <td className={coin.price_change_percentage_24h > 0 ? 'text-success' : 'text-danger' }>
                {Math.round(coin.price_change_percentage_24h*100)/100} %
            </td>
        </tr>

    )
}
export default CoinRow