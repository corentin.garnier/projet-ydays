import {React, useEffect, useState} from 'react';
import  axios from 'axios';
import { useSearchParams } from "react-router-dom";
import HistoryChart from "./HistoryChart";
const CoinCard = () => {

    const [searchParams, setSearchParams] = useSearchParams();
    const [details, setDetails] = useState([]);
    const [coinData, setCoinData] = useState([]);
    const id = searchParams.get("token");


    useEffect(() => {
        getExchange();
        fetchDatas();
    }, []);

    const formatData = (data) => {
        return data.map(el => {
            return {
                t: el[0],
                y: el[1].toFixed(2)
            }
        })
    };

    //API
    const getExchange = async () => {
        await axios
            .get(
            "https://api.coingecko.com/api/v3/coins/"+id+"?tickers=true&market_data=true&community_data=true&developer_data=true&sparkline=true",
                {
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }
            )
            .then((res) => {
                setDetails(res.data);
            });
    };

    const fetchDatas = async () => {
        await axios
            .get(
                "https://api.coingecko.com/api/v3/coins/"+id+"/market_chart?vs_currency=usd&days=7&interval=1",
                {
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }
            )
            .then((res) => {
                setCoinData(formatData(res.data.prices))
                console.log(coinData)
            });
    };



    return (
        <div>
            <a href="/" class="button button4"> ← Retour</a>
            <h2 className="text-center text-uppercase text-decoration-underline my-4">{details.name}</h2>
            <div style={{backgroundColor:'#e6e4e1',borderRadius:'8px',boxShadow:'10px 7px 5px #737475'}} className="mx-4 px-4 py-2">
                <p><strong>Token :</strong> <span className="text-uppercase">{details.symbol}</span></p>
                <p><strong>Market cap rank :</strong> {details.market_cap_rank}</p>
                {details.description ?
                    <p><strong>Description :</strong> {details.description['en']}</p>
                    :
                    <></>
                }
                <p><strong>Dernière mise à jour :</strong> {details.last_updated}</p>
                {details.links ?
                    <p><strong>Lien github :</strong> <a href={details.links.repos_url.github[0]} target="_blank">{details.links.repos_url.github[0]}</a></p>
                    :
                    <></>
                }
            </div>
            <HistoryChart data={coinData} id={id} />
        </div>

        
    )
}
export default CoinCard