import React from 'react';
import  axios from 'axios';
import { useEffect, useState } from "react";

function Exchange () {
    const [darkMode] = React.useState(getInitialMode());

    function getInitialMode() {
        const savedMode = JSON.parse(localStorage.getItem("dark"));
        return savedMode || false;
    }

    const [exchanges, setExchanges] = useState([]);

    const filteredExchanges = exchanges.filter((exchange) =>
    exchange.name.toLowerCase()
  );

    //API
    const getExchange = async () => {
        const res = await axios.get(
          "https://api.coingecko.com/api/v3/exchanges?per_page=10"
        )
        setExchanges(res.data);
        console.log(res.data);
    };
    useEffect(() => {
        getExchange();
      }, []);

    return(
        <div className="mt-5 mx-2 w-100 align-self-baseline " >
            <h4>Liste des exchanges</h4>
            {filteredExchanges.map((exchange, index) => (
                <div className="mx-2 exchange">
                    <div className="item d-flex flex-column justify-content-center my-3 rounded p-3  ">
                        <span key={index} ><a className='text-decoration-none' style={{ color : darkMode ? "white" : "black"}} href={exchange.url} target="_blank">
                            <img 
                                src={exchange.image} 
                                alt="{exchange.name}" 
                                className="me-4"
                            />
                        <span>{exchange.name}</span>
                        {exchange.year_established !== null ?
                        <p className='mt-2'>Date de création : {exchange.year_established}</p>
                        :
                        <p></p>
                        }
                        </a></span>
                    </div>
                </div>
            ))}
        </div>
    )
}
export default Exchange