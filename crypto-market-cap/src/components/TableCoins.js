import React from "react";
import CoinRow from "./CoinRow";

const titles = [" ", "Coin", "Price", "Market Cap", "Price Change - 24h"];
const TableCoins = ({ coins, search, currency}) => {

  const filteredCoins = coins.filter((coin) =>
    coin.name.toLowerCase().includes(search.toLowerCase())
  );
console.log(coins)
  return (
    <div className="padding">
      <table className="table table-striped mt-4 tableau">
        <thead >
          <tr>
              {titles.map((title, index) => (
                  <td key={index}>{title}</td>
              ))}
          </tr>
        </thead>
        <tbody>
            {filteredCoins.map((coin, index) => (
                <CoinRow 
                  coin={coin} 
                  key={index} 
                  index={index + 1}
                  currency={currency}
                />
            ))}
        </tbody>
      </table>
    </div>
  );
};

export default TableCoins;