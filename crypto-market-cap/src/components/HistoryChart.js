import React, { useRef, useEffect, useState } from "react";
import Chartjs from "chart.js";
import { historyOptions } from "../chartConfigs/ChartConfigs"

const HistoryChart = ({ data, id }) => {
    const chartRef = useRef();
    const [timeFormat, setTimeFormat] = useState("24h");


    useEffect(() => {
        if (chartRef && chartRef.current) {
            const chartInstance = new Chartjs(chartRef.current, {
                type: "line",
                data: {
                    datasets: [
                        {
                            label: id+" price",
                            data: data,
                            backgroundColor: "grey",
                            borderColor: "green",
                            pointRadius: 0,
                        },
                    ],
                },
                options: {
                    ...historyOptions,
                },
            });
        }
    });


    return (
        <div className="bg-white border mt-2 rounded m-4 p-3">
            <div>
                <canvas ref={chartRef} id="myChart" width={250} height={250}></canvas>
            </div>

            <div className="chart-button mt-1">
                <button
                    onClick={() => setTimeFormat("24h")}
                    className="btn btn-outline-secondary btn-sm"
                >
                    24h
                </button>
                <button
                    onClick={() => setTimeFormat("7d")}
                    className="btn btn-outline-secondary btn-sm mx-1"
                >
                    7d
                </button>
                <button
                    onClick={() => setTimeFormat("1y")}
                    className="btn btn-outline-secondary btn-sm"
                >
                    1y
                </button>
            </div>
        </div>
    );
};

export default HistoryChart;