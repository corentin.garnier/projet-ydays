import React from 'react';
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Home from "./pages/Home";
import "./App.css";
import CoinCard from './components/CoinCard';

function App() {

  return (
      <div>
        <Router>
          <Routes>
            <Route path="/" element={<Home/>} />
            <Route path="/paire" element={<CoinCard/>} />
          </Routes>
        </Router>
      </div>
  );
}

export default App;
