import Exchange from "../components/Exchange";
import TableCoins from "../components/TableCoins";
import axios from "axios";
import {useEffect, useState } from "react";
import React from 'react';
const Home = () => {

    const [coins, setCoins] = useState([]);
    const [search, setSearch] = useState("");
    const [currency, setCurrency] = useState(getCurrency());

    const [darkMode, setDarkMode] = React.useState(getInitialMode());
    // Garder le mode choisi lors d'un refresh de page
    React.useEffect(() => {
        localStorage.setItem("dark", JSON.stringify(darkMode));
        localStorage.setItem("usd", JSON.stringify(currency));
    }, [darkMode,currency])

    function getInitialMode() {
        const savedMode = JSON.parse(localStorage.getItem("dark"));
        return savedMode || false;
    }

    function getCurrency() {
        const savedCurrency = JSON.parse(localStorage.getItem("usd"));
        return savedCurrency || false;
    }

    //API
    const getData = async () => {
        const res = await axios.get(
            "https://api.coingecko.com/api/v3/coins/markets?vs_currency="+currency+"&order=market_cap_desc&per_page=100&page=1&sparkline=false"
        )
        setCoins(res.data);
    };


    useEffect(() => {
        getData();
        setCurrency(currency)
    }, [currency]);

    return (
        <div className={darkMode ? "dark-mode" : "light-mode"}>
            <div>
                <div className='toggle-container'>
                    <span >💡</span>
                    <span className='toggle'>
              <input
                  checked={darkMode}
                  onChange={() => setDarkMode(prevMode => !prevMode)}
                  type="checkbox"
                  className="checkbox"
                  id="checkbox"
              />
              <label htmlFor="checkbox" />
            </span>
                    <span>🌙</span>
                </div>
            </div>

            <h1 className='h1 text-center mt-4'>Crypto Market Cap</h1>
            <div className='d-flex flex-row mx-3'>
                <div className={darkMode ? "dark-mode" : "light-mode"}>
                    <Exchange />
                </div>
                <div className="container w-75">
                    <div className='row'>
                        <div className="d-flex">
                            <input 
                                type="text"
                                placeholder="Search a Coin" 
                                className='search' 
                                autoFocus
                                onChange={(e) => setSearch(e.target.value)}
                            />
                            <select className="form form-select currency-select ms-4" onChange={(e) => setCurrency(e.target.value)}>
                                <option selected={currency === 'usd' ? true : false} value="usd">USD</option>
                                <option selected={currency === 'eur' ? true : false} value="eur">EUR</option>
                            </select>
                        </div>
                        <div className={darkMode ? "dark-mode" : "light-mode"}>
                            <TableCoins coins={coins} search={search} currency={currency}/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default Home;